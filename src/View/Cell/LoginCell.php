<?php
namespace UserManager\View\Cell;

use Cake\View\Cell;
use UserManager\Lib\GoogleSdk;

/**
 * Login cell
 */
class LoginCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display() {
        
    }
}
